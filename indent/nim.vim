" Only load this indent file when no other was loaded.
if exists('b:did_indent')
  finish
endif
let b:did_indent = 1

" Some preliminary settings
setlocal nolisp         " Make sure lisp indenting doesn't supersede us
setlocal autoindent     " indentexpr isn't much help otherwise

setlocal indentexpr=nim#indent(v:lnum)
setlocal indentkeys=!^F,o,O,<:>,0),0],0},=elif

" vim:sw=2
