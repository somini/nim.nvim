# Basic Nim support for Neovim

This provides basic neovim support for [Nim](http://nim-lang.org) language:

* Syntax highlighting
* Indentation
* Compiler

This is a fork of https://github.com/zah/nim.vim that:
- Eliminates all IDE-like features. The makes the plugin fast, doesn't require Python and does not execute potentially untrusted code on opening files.
   - See https://github.com/zah/nim.vim/issues/87 for example
- Modernises the code to eliminate checks for older vim versions
- Fixes all issues found by [vint](https://github.com/Vimjas/vint)

This is only tested on the latest stable neovim, Merge Requests that restore
support for modern vim versions are welcome.
