if exists('b:nim_loaded')
  finish
endif

let b:nim_loaded = 1

setlocal formatoptions-=t formatoptions+=croql
setlocal comments=:##,:# commentstring=#\ %s
setlocal suffixesadd=.nim
setlocal foldmethod=indent

setlocal expandtab tabstop=2 softtabstop=2 shiftwidth=2

compiler nim
